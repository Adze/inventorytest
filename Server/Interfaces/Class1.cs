﻿using System;
using System.Collections.Generic;

namespace Interfaces
{
    public interface ICommon
    {
        string[] GetAvailableUsers();
        void ChangePrivileges(string Login, string password);
    }

    public interface IAgent
    {
        void Transfer_ComputerName(string _WMI_Data);
        void Transfer_DiskDrive(string _WMI_Data);
        void Transfer_Processor(string _WMI_Data);
        void Transfer_CDROMDrive(string _WMI_Data);
        void Transfer_Motherboard(string _WMI_Data);
        void Transfer_Monitor(string _WMI_Data);
        void Transfer_NetworkAdapter(string _WMI_Data);
        void Transfer_Video(string _WMI_Data);
        void Transfer_RAM(string _WMI_Data);
        void Transfer_Comp(string _WMI_Data);
    }

    public interface IAdmin
    {
        List<string> GetComputerList();
        string GetComputer(string Comp_Name);
        void SaveChanges(string Comp_Name, string Inventory, string Comment);
        void DeleteComputer(string Comp_Name);
        string GetHistory(string Comp_Name);
    }

    public interface IInvent
    {
        bool Transfer_InventNumber(string[] InvNum);
    }

    [Serializable]
    public class Message
    {
        public Message(string Command, object[] Parameters)
        {
            this.Command = Command;
            if (Parameters != null)
                this.prms = Parameters;
        }

        public bool IsSync;
        public bool IsEmpty = true;
        public readonly string Command;
        public object ReturnValue;
        public object[] prms;
        public Exception Exception;
    }
}
