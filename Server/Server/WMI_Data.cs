﻿using System;
using System.Collections.Generic;

namespace WMI_NS
{
    public struct History
    {
        public string Event;
        public string Date;
        public string Description;

        public History(string _Event, string _Date, string _Description)
        {
            Event = _Event;
            Date = _Date;
            Description = _Description;
        }
    }

    /// <summary>
    /// Даные по HDD
    /// <para><value>string Name - имя диска (Caption)</value></para>
    /// <para><value>string Serial - серийный номер диска (SerialNumber)</value></para>
    /// </summary>
    public struct DiskDrive
    {
        public string Name;
        public string Serial;

        public DiskDrive(string _Name, string _Serial)
        {
            Name = _Name;
            Serial = _Serial;
        }
    }

    /// <summary>
    /// Даные по процессору
    /// <para><value>string Name - имя процессора (Name)</value></para>
    /// <para><value>string ID - серийный номер процессора (ProcessorID)</value></para>
    /// </summary>
    public struct Processor
    {
        public string Name;
        public string ID;

        public Processor(string _Name, string _ID)
        {
            Name = _Name;
            ID = _ID;
        }
    }

    /// <summary>
    /// CD-ROM
    /// <para><value>string Name - имя CD-ROM (Caption)</value></para>
    /// <para><value>string Serial - серийный номер CD-ROM (SerialNumber)</value></para>
    /// </summary>
    public struct CDROMDrive
    {
        public string Name;
        public string Serial;

        public CDROMDrive(string _Name, string _Serial)
        {
            Name = _Name;
            Serial = _Serial;
        }
    }

    /// <summary>
    /// ComputerName
    /// <para><value>string Name - имя компьютера (Name)</value></para>
    /// </summary>
    public struct ComputerName
    {
        public string Name;

        public ComputerName(string _Name)
        {
            Name = _Name;
        }
    }

    /// <summary>
    /// Motherboard
    /// <para><value>string Manufacturer - производитель (Manufacturer)</value></para>
    /// <para><value>string Name - модель (Product)</value></para>
    /// </summary>
    public struct Motherboard
    {
        public string Manufacturer;
        public string Name;

        public Motherboard(string _Manufacturer, string _Name)
        {
            Manufacturer = _Manufacturer;
            Name = _Name;
        }
    }

    /// <summary>
    /// Monitor
    /// <para><value>string ID - модель (PNPDeviceID)</value></para>
    /// </summary>
    public struct Display
    {
        public string ID;

        public Display(string _ID)
        {
            ID = _ID;
        }
    }

    /// <summary>
    /// RAM
    /// <para><value>string Slot - слот (DeviceLocator)</value></para>
    /// <para><value>string Size - размер памяти (Capacity)</value></para>
    /// <para><value>string Serial - серийный номер CD-ROM (SerialNumber)</value></para>
    /// </summary>
    public struct RAM
    {
        public string Slot;
        public string Size;
        public string Serial;

        public RAM(string _Slot, string _Size, string _Serial)
        {
            Slot = _Slot;
            Size = _Size;
            Serial = _Serial;
        }
    }

    /// <summary>
    /// Video
    /// <para><value>string Name - модель (Name)</value></para>
    /// <para><value>string VEN_ID - модель (PNPDeviceID)</value></para>
    /// <para><value>string DEV_ID - модель (PNPDeviceID)</value></para>
    /// </summary>
    public struct Video
    {
        public string Name;
        public string VEN_ID;
        public string DEV_ID;

        public Video(string _Name, string _VEN_ID, string _DEV_ID)
        {
            Name = _Name;
            VEN_ID = _VEN_ID;
            DEV_ID = _DEV_ID;
        }
    }

    /// <summary>
    /// NetworkAdapter
    /// <para><value>string Name - модель (ProductName)</value></para>
    /// <para><value>string MAC - модель (MACAdress)</value></para>
    /// <para><value>string VEN_ID - модель (PNPDeviceID)</value></para>
    /// <para><value>string DEV_ID - модель (PNPDeviceID)</value></para>
    /// </summary>
    public struct NetworkAdapter
    {
        public string Name;
        public string MAC;
        public string VEN_ID;
        public string DEV_ID;

        public NetworkAdapter(string _Name, string _MAC, string _VEN_ID, string _DEV_ID)
        {
            Name = _Name;
            MAC = _MAC;
            VEN_ID = _VEN_ID;
            DEV_ID = _DEV_ID;
        }
    }

    /// <summary>
    /// Software
    /// <para><value>string Name - модель (Caption)</value></para>>
    /// </summary>
    public struct Software
    {
        public string Name;

        public Software(string _Name)
        {
            Name = _Name;
        }
    }

    /// <summary>
    /// Computer
    /// </summary>
    //public struct Computer
    public class Computer
    {
        public ComputerName _ComputerName;
        public Motherboard _Motherboard;
        public Processor _Processor;
        public RAM[] _RAM;
        public Video[] _Video;
        public DiskDrive[] _DiskDrive;
        public CDROMDrive[] _CDROMDrive;
        public NetworkAdapter[] _NetworkAdapter;
        public Display[] _Display;
        public Software[] _Software;
        public string _Comment;
        public string _Inventory;

        public Computer()
        {
        }

        public Computer(ComputerName CN, Motherboard MB, Processor PR, RAM[] RM, Video[] VD, DiskDrive[] DD, CDROMDrive[] CDROM, NetworkAdapter[] NA, Display[] _DP, Software[] SW, string CM, string INV)
        {
            _ComputerName = CN;
            _Motherboard = MB;
            _Processor = PR;
            _RAM = RM;
            _Video = VD;
            _DiskDrive = DD;
            _CDROMDrive = CDROM;
            _NetworkAdapter = NA;
            _Display = _DP;
            _Software = SW;
            _Comment = CM;
            _Inventory = INV;
        }

        public static bool operator ==(Computer A, Computer B)
        {
            if (A._ComputerName.Name != B._ComputerName.Name)
            {
                return false;
            }
            if ((A._Motherboard.Manufacturer != B._Motherboard.Manufacturer) || (A._Motherboard.Name != B._Motherboard.Name))
            {
                return false;
            }
            if ((A._Processor.ID != B._Processor.ID) || (A._Processor.Name != B._Processor.Name))
            {
                return false;
            }
            for (int i = 0; i < A._CDROMDrive.Length; i++)
            {
                if ((A._CDROMDrive[i].Name != B._CDROMDrive[i].Name) || (A._CDROMDrive[i].Serial != B._CDROMDrive[i].Serial))
                {
                    return false;
                }
            }
            for (int i = 0; i < A._DiskDrive.Length; i++)
            {
                if ((A._DiskDrive[i].Name != B._DiskDrive[i].Name) || (A._DiskDrive[i].Serial != B._DiskDrive[i].Serial))
                {
                    return false;
                }
            }
            for (int i = 0; i < A._Display.Length; i++)
            {
                if (A._Display[i].ID != B._Display[i].ID)
                {
                    return false;
                }
            }
            for (int i = 0; i < A._NetworkAdapter.Length; i++)
            {
                if ((A._NetworkAdapter[i].Name != B._NetworkAdapter[i].Name) || (A._NetworkAdapter[i].VEN_ID != B._NetworkAdapter[i].VEN_ID) || (A._NetworkAdapter[i].DEV_ID != B._NetworkAdapter[i].DEV_ID) || (A._NetworkAdapter[i].MAC != B._NetworkAdapter[i].MAC))
                {
                    return false;
                }
            }
            for (int i = 0; i < A._RAM.Length; i++)
            {
                if ((A._RAM[i].Slot != B._RAM[i].Slot) || (A._RAM[i].Size != B._RAM[i].Size) || (A._RAM[i].Serial != B._RAM[i].Serial))
                {
                    return false;
                }
            }
            for (int i = 0; i < A._Video.Length; i++)
            {
                if ((A._Video[i].Name != B._Video[i].Name) || (A._Video[i].VEN_ID != B._Video[i].VEN_ID) || (A._Video[i].DEV_ID != B._Video[i].DEV_ID))
                {
                    return false;
                }
            }
            for (int i = 0; i < A._Software.Length; i++)
            {
                if (A._Software[i].Name != B._Software[i].Name)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool operator !=(Computer A, Computer B)
        {
            return !(A == B);
        }
    }
}
