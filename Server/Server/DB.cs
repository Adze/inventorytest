﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using WMI_NS;
using Newtonsoft;

namespace Server
{
    public class DB
    {
        public NpgsqlConnection Connection;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();

        public string Connect(string Params)
        {
            Connection = new NpgsqlConnection(Params);
            try
            {
                Connection.Open();
            }
            catch (Exception msg)
            {
                return msg.Message.ToString();
                throw;
            }
            return "OK";
        }

        public string Disconnect()
        {
            try
            {
                Connection.Close();
            }
            catch (Exception msg)
            {
                return msg.ToString();
                throw;
            }
            return "OK";
        }

        public bool IsConnect()
        {
            if (Connection.State.ToString() == "Open")
                return true;
            else
                return false;
        }

        private DataTable ExecQuery(string query)
        {
            DataTable DT = null;
            try
            {
                DataSet DS = new DataSet();
                NpgsqlDataAdapter SQL = new NpgsqlDataAdapter(query, Connection);
                SQL.Fill(DS);
                DT = DS.Tables[0];

                return DT;
            }
            catch (NpgsqlException ex)
            {
                return DT;
            }
        }

        private NpgsqlDataReader ExecCommand(string query)
        {
            NpgsqlDataReader DR = null;
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(query, Connection);
                DR = command.ExecuteReader();

                return DR;
            }
            catch( NpgsqlException ex)
            {
                return DR;
            }
        }

        private bool ExecCommandNonQuery(string query)
        {
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(query, Connection);
                command.ExecuteNonQuery();
                return true;
            }
            catch (NpgsqlException ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
                return false;
            }
        }

        public int GetComputerID(bool IsName, string ident)
        {
            int result = 0;
            string query = String.Empty;
            if (IsName)
            {
                query = String.Format("SELECT id FROM computer WHERE name = '{0}'", ident);
            }
            else
            {
                query = String.Format("SELECT id FROM computer WHERE inventory_number = '{0}'", ident);
            }
            try
            {
                NpgsqlDataReader DR = ExecCommand(query);

                while (DR.Read())
                {
                    int.TryParse(DR[0].ToString(), out result);
                }
                
                return result;
            }
            catch (NpgsqlException ex)
            {
                return result;
            }
        }

        public List<DiskDrive> GetDiskDrive(int Comp_ID)
        {
            List<DiskDrive> _DD = null;

            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT diskdrive FROM computer WHERE id = {0}", Comp_ID));
                string diskdrive = String.Empty;
                while (DR.Read())
                {
                    diskdrive = DR[0].ToString();
                }
                _DD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiskDrive>>(diskdrive);
                
                return _DD;
            }
            catch (NpgsqlException ex)
            {
                return _DD;
            }
        }

        public List<Processor> GetProcessor(int Comp_ID)
        {
            List<Processor> _PR = null;

            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT processor FROM computer WHERE id = {0}", Comp_ID));
                string processor = String.Empty;
                while (DR.Read())
                {
                    processor = DR[0].ToString();
                }
                _PR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Processor>>(processor);

                return _PR;
            }
            catch (NpgsqlException ex)
            {
                return _PR;
            }
        }

        public List<CDROMDrive> GetCDROM(int Comp_ID)
        {
            List<CDROMDrive> _CD = null;

            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT cdromdrive FROM computer WHERE id = {0}", Comp_ID));
                string cdrom = String.Empty;
                while (DR.Read())
                {
                    cdrom = DR[0].ToString();
                }
                _CD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CDROMDrive>>(cdrom);

                return _CD;
            }
            catch (NpgsqlException ex)
            {
                return _CD;
            }
        }

        public Motherboard GetMotherboard(int Comp_ID)
        {
            Motherboard _MB = new Motherboard();

            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT motherboard FROM computer WHERE id = {0}", Comp_ID));

                return _MB;
            }
            catch (NpgsqlException ex)
            {
                return _MB;
            }
        }

        public List<Display> GetDisplay(int Comp_ID)
        {
            List<Display> result = null;
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT monitor FROM computer WHERE name = '{0}'", Comp_ID));
                string display = String.Empty;
                while (DR.Read())
                {
                    display = DR[0].ToString();
                }
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Display>>(display);

                return result;
            }
            catch (NpgsqlException ex)
            {
                return result;
            }
        }

        public List<RAM> GetRAM(int Comp_ID)
        {
            List<RAM> result = null;
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT ram FROM computer WHERE name = '{0}'", Comp_ID));
                string ram = String.Empty;
                while (DR.Read())
                {
                    ram = DR[0].ToString();
                }
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RAM>>(ram);

                return result;
            }
            catch (NpgsqlException ex)
            {
                return result;
            }
        }

        public List<Video> GetVideo(int Comp_ID)
        {
            List<Video> result = null;
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT video FROM computer WHERE name = '{0}'", Comp_ID));
                string video = String.Empty;
                while (DR.Read())
                {
                    video = DR[0].ToString();
                }
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Video>>(video);

                return result;
            }
            catch (NpgsqlException ex)
            {
                return result;
            }
        }

        public List<NetworkAdapter> GetNetworkAdapter(int Comp_ID)
        {
            List<NetworkAdapter> result = null;
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT video FROM computer WHERE name = '{0}'", Comp_ID));
                string na = String.Empty;
                while (DR.Read())
                {
                    na = DR[0].ToString();
                }
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NetworkAdapter>>(na);

                return result;
            }
            catch (NpgsqlException ex)
            {
                return result;
            }
        }

        public List<History> GetHistory(string Comp_Name)
        {
            List<History> result = null;
            int Comp_ID = GetComputerID(true, Comp_Name);
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format(@"SELECT event.NAME, history.DATE, history.description FROM event, history WHERE history.event_id = event.id AND history.computer_id = {0}", Comp_ID));
                result = new List<History>();
                while (DR.Read())
                {
                    result.Add(new History(DR[0].ToString(), DR[1].ToString(), DR[2].ToString()));
                }

                return result;
            }
            catch (NpgsqlException ex)
            {
                return result;
            }
        }

        public List<Software> GetSoftwareList(int Comp_ID)
        {
            List<Software> result = null;
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format(@"SELECT software.NAME FROM software, software_list WHERE software_list.""Software_id"" = software.id AND software_list.""Computer_id"" = {0}", Comp_ID));
                result = new List<Software>();
                while (DR.Read())
                {
                    result.Add(new Software(DR[0].ToString()));
                }

                return result;
            }
            catch (NpgsqlException ex)
            {
                return result;
            }
        }

        public int GetSoftwareID(string Soft)
        {
            int index = 0;
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("SELECT id FROM software WHERE name = '{0}'", Soft));
                while (DR.Read())
                {
                    index = int.Parse(DR[0].ToString());
                }

                return index;
            }
            catch (NpgsqlException ex)
            {
                return index;
            }
        }

        public int SetSoftware(string Soft)
        {
            int index = 0;
            try
            {
                NpgsqlDataReader DR = ExecCommand(String.Format("INSERT INTO software (name) VALUES('{0}') RETURNING id", Soft));
                while (DR.Read())
                {
                    index = int.Parse(DR[0].ToString());
                }

                return index;
            }
            catch (NpgsqlException ex)
            {
                return index;
            }
        }

        public bool AddSoftwareList(int Soft_ID, int Comp_ID)
        {
            if (ExecCommandNonQuery(String.Format(@"INSERT INTO software_list (""Computer_id"", ""Software_id"") VALUES ({0}, {1})", Comp_ID, Soft_ID)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteSoftwareList(string Soft, int Comp_ID)
        {
            if (ExecCommandNonQuery(String.Format("DELETE FROM software_list WHERE Computer_id={0} AND Software_id=(SELECT id FROM software WHERE name='{1}')", Comp_ID, Soft)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool GetComp(string CompName, out Computer ResultComp)
        {
            Computer Comp = new Computer();
            try
            {
                Comp._ComputerName = new ComputerName(CompName);
                int ID = GetComputerID(true, CompName);

                DataTable DT = ExecQuery(String.Format("SELECT * FROM computer WHERE id = {0}", ID));

                Comp._CDROMDrive = GetListFromJSON<CDROMDrive>(DT.Rows[0]["cdrom"].ToString()).ToArray();
                Comp._DiskDrive = GetListFromJSON<DiskDrive>(DT.Rows[0]["diskdrive"].ToString()).ToArray();
                Comp._Motherboard = GetDataFromJSON<Motherboard>(DT.Rows[0]["motherboard"].ToString());
                Comp._NetworkAdapter = GetListFromJSON<NetworkAdapter>(DT.Rows[0]["networkadapter"].ToString()).ToArray();
                Comp._Processor = GetDataFromJSON<Processor>(DT.Rows[0]["processor"].ToString());
                Comp._RAM = GetListFromJSON<RAM>(DT.Rows[0]["ram"].ToString()).ToArray();
                Comp._Video = GetListFromJSON<Video>(DT.Rows[0]["video"].ToString()).ToArray();
                Comp._Display = GetListFromJSON<Display>(DT.Rows[0]["monitor"].ToString()).ToArray();
                Comp._Software = GetSoftwareList(ID).ToArray();
                Comp._Comment = DT.Rows[0]["comment"].ToString();
                Comp._Inventory = DT.Rows[0]["inventory_number"].ToString();

                ResultComp = Comp;
                return true;
            }
            catch (NpgsqlException ex)
            {
                ResultComp = Comp;
                return false;
            }
        }

        public bool SetComp(Computer Comp)
        {
            if (ExecCommandNonQuery(String.Format("INSERT INTO computer (name, motherboard, processor, ram, diskdrive, cdrom, video, networkadapter, monitor)" +
                "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{6}', '{7}')",
                Comp._ComputerName.Name, Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Motherboard, Newtonsoft.Json.Formatting.Indented), Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Processor, Newtonsoft.Json.Formatting.Indented),
                Newtonsoft.Json.JsonConvert.SerializeObject(Comp._RAM, Newtonsoft.Json.Formatting.Indented), Newtonsoft.Json.JsonConvert.SerializeObject(Comp._DiskDrive, Newtonsoft.Json.Formatting.Indented), Newtonsoft.Json.JsonConvert.SerializeObject(Comp._CDROMDrive, Newtonsoft.Json.Formatting.Indented),
                Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Video, Newtonsoft.Json.Formatting.Indented), Newtonsoft.Json.JsonConvert.SerializeObject(Comp._NetworkAdapter, Newtonsoft.Json.Formatting.Indented), Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Display, Newtonsoft.Json.Formatting.Indented))))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdComp(Computer Comp, int ID)
        {
            string test = "UPDATE computer SET name='" + 
                Comp._ComputerName.Name + "', motherboard='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Motherboard, Newtonsoft.Json.Formatting.Indented) + 
                "', processor='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Processor, Newtonsoft.Json.Formatting.Indented) + 
                "', ram='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._RAM, Newtonsoft.Json.Formatting.Indented) + 
                "', diskdrive='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._DiskDrive, Newtonsoft.Json.Formatting.Indented) + 
                "', cdrom='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._CDROMDrive, Newtonsoft.Json.Formatting.Indented) + 
                "', video='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Video, Newtonsoft.Json.Formatting.Indented) + 
                "', networkadapter='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._NetworkAdapter, Newtonsoft.Json.Formatting.Indented) + 
                "', monitor='" + Newtonsoft.Json.JsonConvert.SerializeObject(Comp._Display, Newtonsoft.Json.Formatting.Indented) + "' WHERE id=" + ID;
            if (ExecCommandNonQuery(test))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetEvent(int ID, int event_id, string description)
        {
            string query = String.Format("INSERT INTO history (event_id, computer_id, description, date) VALUES ({0}, {1}, '{2}', '{3}')", event_id, ID, description, DateTime.Now);

            if (ExecCommandNonQuery(query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetInventoryNumber(int event_id, string[] InData)
        {
            bool result = false;

            for (int i = 1; i < InData.Length; i++)
            {
                result = SetEvent(GetComputerID(false, InData[i]), event_id, InData[0]);
            }
            return result;
        }

        public List<string> GetComputerList()
        {
            List<string> ComputerList = new List<string>();

            try
            {
                NpgsqlDataReader DR = ExecCommand("SELECT name FROM computer");
                while (DR.Read())
                {
                    ComputerList.Add(DR[0].ToString());
                }
            }
            catch (NpgsqlException ex)
            {
                return null;
            }
            return ComputerList;
        }

        public bool SaveChanges(string Comp_Name, string Invenotry, string Comment)
        {
            string query = String.Format("UPDATE computer set inventory_number='{0}', comment='{1}' WHERE name = '{2}'", Invenotry, Comment, Comp_Name);

            if (ExecCommandNonQuery(query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteComputer(string Comp_Name)
        {
            string query = String.Format("DELETE FROM computer WHERE name='{0}'", Comp_Name);

            if (ExecCommandNonQuery(query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private List<T> GetListFromJSON<T>(string json) where T : struct
        {
            List<T> result = null;
            result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<T>>(json);

            return result;
        }

        private T GetDataFromJSON<T>(string json) where T : struct
        {
            T result;
            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);

            return result;
        }
    }
}
