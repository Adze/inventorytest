﻿using System;
using System.Collections.Generic;
using Log;
using WMI_NS;

namespace Server
{
    public static class Errors
    {
        public delegate void ErrorEvent(string ErrorMsg);

        public static ErrorEvent ErrorEventHandler;
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Log LogFile = new Log.Log();
            Server_class server = new Server_class(2000);
            
			server.Start();
			
            server.ConnectToDB();
        }
    }
}
