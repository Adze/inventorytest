﻿#define USE_COMPRESSION
//#define USE_JSON

using Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using Newtonsoft;
using WMI_NS;

namespace Server
{
    public enum UserType
    {
        Unautorized,
        Agent,
        Admin,
        IInvent,
    }

    class Server_class
    {
        public const int PING_TIME = 7000;
        public System.Net.Sockets.TcpListener SERV;
        static readonly SyncAccess ConnectedUsers = new SyncAccess();
        private static DB DataBase = new DB();

        public bool ConnectToDB()
        {
            string ConnectionString = String.Format("Server=127.0.0.1;Port=5433;User Id=postgres;Password=u7Nef3fi9Nfweu;Database=json-test;");
            string Status = DataBase.Connect(ConnectionString);
            if (Status == "OK")
            {Console.WriteLine("Подключение к базе данных установлено");
                return true;
            }
            else
            {Console.WriteLine("Ошибка подключения к базе данных");
                Errors.ErrorEventHandler(Status);
                return false;
            }
        }

        #region Синхронный доступ к юзерам

        private class SyncAccess
        {
            private List<User> userList = new List<User>();
            private readonly object listLock = new object();

            public void Add(User item)
            {
                lock (listLock)
                {
                    userList.Add(item);
                }
            }

            public bool Remove(User up)
            {
                lock (listLock)
                {
                    up.Dispose();
                    return userList.Remove(up);
                }
            }

            public User[] ToArray()
            {
                lock (listLock)
                {
                    return userList.ToArray();
                }
            }
        }
        #endregion

        #region Юзер
        public class User : IDisposable
        {
            public User(TcpClient Socket)
            {
                this._socket = Socket;
                Socket.ReceiveTimeout = PING_TIME * 4;
                Socket.SendTimeout = PING_TIME * 4;
                nStream = new ConqurentNetworkSream(Socket.GetStream());
                _pingTimer = new Timer(OnPing, null, PING_TIME, PING_TIME);
                ClassInstance = new Ring2(this);
            }

            private void OnPing(object state)
            {
                SendMessage(nStream, new Message("OnPing", null));
            }

            private readonly Timer _pingTimer;
            public Type RingType { get; private set; }
            private Ring _ClassInstance;
            public Ring ClassInstance
            {
                get { return _ClassInstance; }
                set
                {
                    _ClassInstance = value;
                    RingType = _ClassInstance.GetType();
                }
            }
            public UserType UserType = UserType.Unautorized;

            public byte[] HeaderLenght = BitConverter.GetBytes((int)0);

            private readonly TcpClient _socket;
            public readonly ConqurentNetworkSream nStream;

            private readonly object _disposeLock = new object();
            private bool _IsDisposed = false;
            public void Dispose()
            {
                lock (_disposeLock)
                {
                    if (!_IsDisposed)
                    {
                        _IsDisposed = true;
                        nStream.Dispose();
                        _pingTimer.Dispose();
                        _socket.Close();
                    }
                }
            }
        }
        #endregion

        #region FIFO

        public class ConqurentNetworkSream : IDisposable
        {
            private readonly BlockingCollection<byte[]> _fifo = new BlockingCollection<byte[]>();
            private readonly NetworkStream _nstream;
            private readonly CancellationTokenSource _token = new CancellationTokenSource();
            private readonly ManualResetEventSlim _disposeEvent = new ManualResetEventSlim(false);

            public ConqurentNetworkSream(NetworkStream nstream)
            {
                this._nstream = nstream;
                ThreadPool.QueueUserWorkItem(_thread);
            }

            private void _thread(object state)
            {
                try
                {
                    while (true)
                    {
                        try
                        {
                            byte[] data = _fifo.Take(_token.Token);
                            _nstream.BeginWrite(data, 0, data.Length, null, null);
                        }
                        catch (InvalidOperationException)
                        {
                            continue;
                        }
                    }
                }
                catch (Exception)
                {
                    _disposeEvent.Set();
                    return;
                }
            }

            public void Add(byte[] data)
            {
                _fifo.Add(data);
            }

            public int Read(byte[] data)
            {
                return _nstream.Read(data, 0, data.Length);
            }

            public int EndRead(IAsyncResult asyncResult)
            {
                return _nstream.EndRead(asyncResult);
            }

            public IAsyncResult BeginRead(byte[] data, AsyncCallback callback, object state)
            {
                return _nstream.BeginRead(data, 0, data.Length, callback, state);
            }

            private readonly object _disposeLock = new object();
            private bool _IsDisposed;
            public void Dispose()
            {
                lock (_disposeLock)
                {
                    if (!_IsDisposed)
                    {
                        _IsDisposed = true;
                        _token.Cancel();
                        _disposeEvent.Wait();
                        _token.Dispose();
                        _disposeEvent.Dispose();
                        _nstream.Dispose();
                    }
                }
            }
        }
        #endregion

        public Server_class(int Port)
        {
            SERV = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, Port);
            Console.Title = string.Concat("Порт: ", Port);
        }

        public void Start()
        {
            SERV.Start();
            SERV.BeginAcceptTcpClient(OnAcceptClient, null);
        }

        public void OnAcceptClient(IAsyncResult asyncResult)
        {
            var client = SERV.EndAcceptTcpClient(asyncResult);
            SERV.BeginAcceptTcpClient(OnAcceptClient, null);

            User up = new User(client);
            ConnectedUsers.Add(up);

            try
            {
                up.nStream.BeginRead(up.HeaderLenght, OnDataReadCallback, up);
            }
            catch (IOException)
            {
                ConnectedUsers.Remove(up);
            }
        }

        private void OnDataReadCallback(IAsyncResult asyncResult)
        {
            User up = (User)asyncResult.AsyncState;
            byte[] data;

            try
            {
                up.nStream.EndRead(asyncResult);
                int dataLenght = BitConverter.ToInt32(up.HeaderLenght, 0);
                data = new byte[dataLenght];
                up.nStream.Read(data);

                Message msg = MessageFromBinary<Message>(data);
                ProcessMessage(msg, up);

                up.nStream.BeginRead(up.HeaderLenght, OnDataReadCallback, up);
            }
            catch (Exception)
            {
                ConnectedUsers.Remove(up);
                GC.Collect(2, GCCollectionMode.Optimized);
                return;
            }
        }

        private void ProcessMessage(Message msg, User u)
        {
            string MethodName = msg.Command;
            if (MethodName == "OnPing")
            {
                return;
            }

            MethodInfo method = u.RingType.GetMethod(MethodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);

            try
            {
                if (method == null)
                {
                    throw new Exception(string.Concat("Метод \"", MethodName, "\" недоступен"));
                }

                try
                {
                    msg.ReturnValue = method.Invoke(u.ClassInstance, msg.prms);
                }
                catch (Exception ex)
                {
                    throw ex.InnerException;
                }
				
                msg.prms = method.GetParameters().Select(x => x.ParameterType.IsByRef ? msg.prms[x.Position] : null).ToArray();
            }
            catch (Exception ex)
            {
                msg.Exception = ex;
            }
            finally
            {
                SendMessage(u.nStream, msg);
            }
        }

        #region Кольца

        public class Invent_Ring0 : Ring2, IInvent
        {
            public Invent_Ring0(User u)
                : base(u)
            {
                up.UserType = UserType.IInvent;
            }

            public bool Transfer_InventNumber(string[] InvNum)
            {
                return DataBase.SetInventoryNumber(2, InvNum);
            }
        }

        public class Admin_Ring0 : Ring2, IAdmin
        {
            public Admin_Ring0(User u)
                : base(u)
            {
                up.UserType = UserType.Admin;
            }

            public List<string> GetComputerList()
            {
                List<string> ComputerList = new List<string>();

                ComputerList = DataBase.GetComputerList();

                return ComputerList;
            }

            public string GetComputer(string Comp_Name)
            {
                Computer Comp = null;

                DataBase.GetComp(Comp_Name, out Comp);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Comp);
            }

            public void SaveChanges(string Comp_Name, string Inventory, string Comment)
            {
                DataBase.SaveChanges(Comp_Name, Inventory, Comment);
            }

            public void DeleteComputer(string Comp_Name)
            {
                DataBase.DeleteComputer(Comp_Name);
            }

            public string GetHistory(string Comp_Name)
            {
                List<History> history = DataBase.GetHistory(Comp_Name);
                return Newtonsoft.Json.JsonConvert.SerializeObject(history);
            }
        }

        public class Agent_Ring0 : Ring2, IAgent
        {
            public Agent_Ring0(User u)
                : base(u)
            {
                up.UserType = UserType.Agent;
            }

            public void Transfer_ComputerName(string _WMI_Data)
            {
                ComputerName data = Newtonsoft.Json.JsonConvert.DeserializeObject<ComputerName>(_WMI_Data);
            }

            public void Transfer_DiskDrive(string _WMI_Data)
            {
                List<DiskDrive> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiskDrive>>(_WMI_Data);
            }

            public void Transfer_Processor(string _WMI_Data)
            {
                Processor data = Newtonsoft.Json.JsonConvert.DeserializeObject<Processor>(_WMI_Data);
            }

            public void Transfer_CDROMDrive(string _WMI_Data)
            {
                CDROMDrive data = Newtonsoft.Json.JsonConvert.DeserializeObject<CDROMDrive>(_WMI_Data);
			}

            public void Transfer_Motherboard(string _WMI_Data)
            {
                Motherboard data = Newtonsoft.Json.JsonConvert.DeserializeObject<Motherboard>(_WMI_Data);
            }

            public void Transfer_Monitor(string _WMI_Data)
            {
                Display data = Newtonsoft.Json.JsonConvert.DeserializeObject<Display>(_WMI_Data);
            }

            public void Transfer_NetworkAdapter(string _WMI_Data)
            {
                NetworkAdapter data = Newtonsoft.Json.JsonConvert.DeserializeObject<NetworkAdapter>(_WMI_Data);
            }

            public void Transfer_Video(string _WMI_Data)
            {
                Video data = Newtonsoft.Json.JsonConvert.DeserializeObject<Video>(_WMI_Data);
            }

            public void Transfer_RAM(string _WMI_Data)
            {
                RAM data = Newtonsoft.Json.JsonConvert.DeserializeObject<RAM>(_WMI_Data);
            }

            public void Transfer_Comp(string _WMI_Data)
            {
                Computer Comp = Newtonsoft.Json.JsonConvert.DeserializeObject<Computer>(_WMI_Data);
                int ID = DataBase.GetComputerID(true, Comp._ComputerName.Name);
                if (ID != 0)
                {
                    bool IsChanged = false;

                    Computer CompFromBase;
                    DataBase.GetComp(Comp._ComputerName.Name, out CompFromBase);
                    
                    if ((Comp._Motherboard.Manufacturer != CompFromBase._Motherboard.Manufacturer) || (Comp._Motherboard.Name != CompFromBase._Motherboard.Name))
                    {
                        string description = "Замена " + typeof(Motherboard) + ":" + Comp._Motherboard.Manufacturer + " " + Comp._Motherboard.Name + " на " + typeof(Motherboard) + ":" + CompFromBase._Motherboard.Manufacturer + " " + CompFromBase._Motherboard.Name;
                        DataBase.SetEvent(ID, 1, description);
                        IsChanged = true;
                    }
                    if ((Comp._Processor.ID != CompFromBase._Processor.ID) || (Comp._Processor.Name != CompFromBase._Processor.Name))
                    {
                        DataBase.SetEvent(ID, 1, String.Format("Замена {0}:{1} на {0}:{2}", typeof(Processor), Comp._Processor.Name, CompFromBase._Processor.Name));
                        IsChanged = true;
                    }
                    for (int i = 0; i < Comp._CDROMDrive.Length; i++)
                    {
                        if ((Comp._CDROMDrive[i].Name != CompFromBase._CDROMDrive[i].Name) || (Comp._CDROMDrive[i].Serial != CompFromBase._CDROMDrive[i].Serial))
                        {
                            DataBase.SetEvent(ID, 1, String.Format("Замена {0}:{1} на {0}:{2}", typeof(CDROMDrive), Comp._CDROMDrive[i].Name, CompFromBase._CDROMDrive[i].Name));
                            IsChanged = true;
                        }
                    }
                    for (int i = 0; i < Comp._DiskDrive.Length; i++)
                    {
                        if ((Comp._DiskDrive[i].Name != CompFromBase._DiskDrive[i].Name) || (Comp._DiskDrive[i].Serial != CompFromBase._DiskDrive[i].Serial))
                        {
                            DataBase.SetEvent(ID, 1, String.Format("Замена {0}:{1} на {0}:{2}", typeof(DiskDrive), Comp._DiskDrive[i].Name, CompFromBase._DiskDrive[i].Name));
                            IsChanged = true;
                        }
                    }
                    for (int i = 0; i < Comp._Display.Length; i++)
                    {
                        if (Comp._Display[i].ID != CompFromBase._Display[i].ID)
                        {
                            DataBase.SetEvent(ID, 1, String.Format("Замена {0}:{1} на {0}:{2}", typeof(Display), Comp._Display[i].ID, CompFromBase._Display[i].ID));
                            IsChanged = true;
                        }
                    }
                    for (int i = 0; i < Comp._NetworkAdapter.Length; i++)
                    {
                        if ((Comp._NetworkAdapter[i].Name != CompFromBase._NetworkAdapter[i].Name) || (Comp._NetworkAdapter[i].VEN_ID != CompFromBase._NetworkAdapter[i].VEN_ID) || (Comp._NetworkAdapter[i].DEV_ID != CompFromBase._NetworkAdapter[i].DEV_ID) || (Comp._NetworkAdapter[i].MAC != CompFromBase._NetworkAdapter[i].MAC))
                        {
                            DataBase.SetEvent(ID, 1, String.Format("Замена {0}:{1} на {0}:{2}", typeof(NetworkAdapter), Comp._NetworkAdapter[i].Name, CompFromBase._NetworkAdapter[i].Name));
                            IsChanged = true;
                        }
                    }
                    for (int i = 0; i < Comp._RAM.Length; i++)
                    {
                        if ((Comp._RAM[i].Slot != CompFromBase._RAM[i].Slot) || (Comp._RAM[i].Size != CompFromBase._RAM[i].Size) || (Comp._RAM[i].Serial != CompFromBase._RAM[i].Serial))
                        {
                            DataBase.SetEvent(ID, 1, String.Format("Замена {0}:{1} {2} на {0}:{3} {4}", typeof(RAM), Comp._RAM[i].Size, Comp._RAM[i].Serial, CompFromBase._RAM[i].Size, CompFromBase._RAM[i].Serial));
                            IsChanged = true;
                        }
                    }
                    for (int i = 0; i < Comp._Video.Length; i++)
                    {
                        if ((Comp._Video[i].Name != CompFromBase._Video[i].Name) || (Comp._Video[i].VEN_ID != CompFromBase._Video[i].VEN_ID) || (Comp._Video[i].DEV_ID != CompFromBase._Video[i].DEV_ID))
                        {
                            DataBase.SetEvent(ID, 1, String.Format("Замена {0}:{1} на {0}:{2}", typeof(Video), Comp._Video[i].Name, CompFromBase._Video[i].Name));
                            IsChanged = true;
                        }
                    }
                    if (IsChanged)
                    {
                        DataBase.UpdComp(Comp, ID);
                    }
                    List<Software> Soft_From_Base = DataBase.GetSoftwareList(ID);
                    List<Software> Soft = Comp._Software.ToList();
                    UpdateSoftware(Soft, Soft_From_Base, ID);
                }
                else
                {
                    DataBase.SetComp(Comp);
                    List<Software> Soft = Comp._Software.ToList();
                    int comp_id = DataBase.GetSoftwareID(Comp._ComputerName.Name);
                    UpdateSoftware(Soft, null, ID);
                }
            }

            private void UpdateSoftware(List<Software> Soft, List<Software> Soft_From_Base, int comp_id)
            {

                if (Soft_From_Base != null)
                {
                    foreach (Software soft in Soft)
                    {
                        Soft_From_Base.Remove(soft);
                        Soft.Remove(soft);
                    }
                    foreach (Software soft in Soft_From_Base)
                    {
                        DataBase.DeleteSoftwareList(soft.Name, comp_id);
                    }
                }
                foreach (Software soft in Soft)
                {
                    int index = DataBase.GetSoftwareID(soft.Name);
                    if (index == 0)
                    {
                        index = DataBase.SetSoftware(soft.Name);
                    }
                    else
                    {
                        DataBase.AddSoftwareList(index, comp_id);
                    }
                }          
            }

        }

        public class Ring2 : Ring, ICommon 
        {
            public Ring2(User u)
                : base(u)
            {

            }

            public string[] GetAvailableUsers()
            {
                return new string[] { "Agent", "Admin" };
            }

            public void ChangePrivileges(string User, string password)
            {
                switch (User)
                {
                    case "Agent":
                        if (password != "test_agent") throw new Exception("Не верный пароль");
                        up.ClassInstance = new Agent_Ring0(up);
                        break;
                    case "Admin":
                        if (password != "test_admin") throw new Exception("Не верный пароль");
                        up.ClassInstance = new Admin_Ring0(up);
                        break;
                    case "Invent":
                        if (password != "test_invent") throw new Exception("Не верный пароль");
                        up.ClassInstance = new Invent_Ring0(up);
                        break;
                    default:
                        throw new Exception("Такого пользователя не существует");
                }
            }
        }

        public abstract class Ring
        {
            public readonly User up;

            public Ring(User up)
            {
                this.up = up;
            }
        }
        #endregion

        #region Send/Recive

        private T MessageFromBinary<T>(byte[] BinaryData) where T : class
        {
#if USE_COMPRESSION
            using (MemoryStream memory = new MemoryStream(BinaryData))
            {
                using (var gZipStream = new GZipStream(memory, CompressionMode.Decompress, false))
                {
#if USE_JSON
                    using (Newtonsoft.Json.Bson.BsonReader reader = new Newtonsoft.Json.Bson.BsonReader(memory))
                    {
                        Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
                        T temp = serializer.Deserialize<T>(reader);
                        return temp;
                    }
#else

                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    return (T)binaryFormatter.Deserialize(memory);

#endif
                }
            }
#else
            using (MemoryStream memory = new MemoryStream(BinaryData))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                return (T)binaryFormatter.Deserialize(memory);
            }
#endif
        }

        private static void SendMessage(ConqurentNetworkSream nStream, Message msg)
        {
#if USE_COMPRESSION
            using (MemoryStream memory = new MemoryStream())
            {
                using(var gZipStream = new GZipStream(memory, CompressionMode.Compress, false))
                {
#if USE_JSON
                    using (Newtonsoft.Json.Bson.BsonWriter writer = new Newtonsoft.Json.Bson.BsonWriter(memory))
                    {
                        Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
                        serializer.Serialize(writer, msg);
                    }
#else
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        binaryFormatter.Serialize(memory, msg);

#endif
                }

                byte[] BinaryData = memory.ToArray();
                byte[] DataLenght = BitConverter.GetBytes(BinaryData.Length);
                byte[] DataWithHeader = DataLenght.Concat(BinaryData).ToArray();

                nStream.Add(DataWithHeader);
            }
#else
            using (MemoryStream memory = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memory, msg);
                nStream.Add(memory.ToArray());
            }
#endif
        }

        #endregion
    }
}
