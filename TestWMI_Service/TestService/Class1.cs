﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Management;
using System.Management.Instrumentation;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading;

namespace TestService
{
    class Class1 : System.ServiceProcess.ServiceBase
    {
        static void Main()
        {
            ServiceBase[] ServiceToRun;
            ServiceToRun = new ServiceBase[] { new Class1() };
            ServiceBase.Run(ServiceToRun);
        }

        private void InitializeComponent()
        {
            this.ServiceName = "WinService4";
        }

        private string folderPath = @"D:\!Share\Scans";

        protected override void OnStart(string[] args)
        {
            if (!System.IO.Directory.Exists(folderPath))
            {
                System.IO.Directory.CreateDirectory(folderPath);
            }

            FileStream fs = new FileStream(folderPath + "\\WindowsService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter m_streamWriter = new StreamWriter(fs);
            m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            m_streamWriter.WriteLine("WindowsService: Service Started at " + 
                DateTime.Now.ToShortDateString() + " " + 
                DateTime.Now.ToShortTimeString() + "\n");
            string Query = "SELECT * FROM Win32_DiskDrive";
            string result = string.Empty;
            ManagementObjectSearcher seacher = new ManagementObjectSearcher(Query);
            foreach (ManagementObject obj in seacher.Get())
            {
                result = obj["Caption"].ToString().Trim();
                m_streamWriter.WriteLine("Hard Disk Drive Caption: " + result);
                result = obj["SerialNumber"].ToString().Trim();
                m_streamWriter.WriteLine("Hard Disk Drive SerialNumber: " + result);
            }
            m_streamWriter.Flush();
            Query = "SELECT * FROM Win32_Processor";
            result = string.Empty;
            seacher = new ManagementObjectSearcher(Query);
            foreach (ManagementObject obj in seacher.Get())
            {
                result = obj["Name"].ToString().Trim();
                m_streamWriter.WriteLine("Processor Name: " + result);
                result = obj["ProcessorID"].ToString().Trim();
                m_streamWriter.WriteLine("Processor ID: " + result);
            }
            m_streamWriter.Flush();
            Query = "SELECT * FROM Win32_Product";
            result = string.Empty;
            seacher = new ManagementObjectSearcher(Query);
            foreach (ManagementObject obj in seacher.Get())
            {
                result = obj["Caption"].ToString().Trim();
                m_streamWriter.WriteLine(result);
            }
            m_streamWriter.Flush();
            m_streamWriter.Close();
        }

        protected override void OnStop()
        {
            FileStream fs = new FileStream(folderPath + "\\WindowsService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter m_streamWriter = new StreamWriter(fs);
            m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            m_streamWriter.WriteLine("WindowsService: Service Stopped at " +
                DateTime.Now.ToShortDateString() + " " +
                DateTime.Now.ToShortTimeString() + "\n");
            m_streamWriter.Flush();
            m_streamWriter.Close();
        }
    }
}
