﻿using System;
using System.Management;
using System.Management.Instrumentation;
using Microsoft.Win32;
using System.Runtime.InteropServices;

namespace TestWMI
{
    class Program
    {
        static void Main(string[] args)
        {
            string Query = "SELECT Caption FROM Win32_DiskDrive";
            string result = string.Empty;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            foreach (ManagementObject obj in searcher.Get())
            {
                result = obj["Caption"].ToString().Trim();
                Console.WriteLine(result);
            }
            Console.ReadKey();
        }
    }
}
