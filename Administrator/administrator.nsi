; Script generated with the Venis Install Wizard

; Define your application name
!define APPNAME "Administrator"
!define APPNAMEANDVERSION "Administrator 1.0"

; Main Install settings
Name "${APPNAMEANDVERSION}"
InstallDir "$PROGRAMFILES\Administrator"
InstallDirRegKey HKLM "Software\${APPNAME}" ""
OutFile "Administrator_Setup.exe"

; Modern interface settings
!include "MUI.nsh"

!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH


!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Set languages (first is default language)
!insertmacro MUI_LANGUAGE "Russian"
!insertmacro MUI_RESERVEFILE_LANGDLL

Var "Server"
Var "Port"
Var "User"
Var "Password"

Function .onInit
   initpluginsdir
   file /oname=$PLUGINSDIR\custom.ini custom.ini
   Push $0
FunctionEnd

Section "Agent" Section1

	; Set Section properties
	SetOverwrite on

	; Set Section Files and Shortcuts
	SetOutPath "$INSTDIR\"
	File "Administrator\bin\Debug\Administrator.exe"
	File "Administrator\bin\Debug\Interfaces.dll"
	File "Administrator\bin\Debug\Newtonsoft.Json.dll"
	File "Administrator\bin\Debug\ThoughtWorks.QRCode.dll"
	CreateDirectory "$SMPROGRAMS\Administrator"
	CreateShortCut "$SMPROGRAMS\Administrator\Uninstall.lnk" "$INSTDIR\uninstall.exe"

SectionEnd

Section -FinishSection

	WriteRegStr HKLM "Software\${APPNAME}" "" "$INSTDIR"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "DisplayName" "${APPNAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "UninstallString" "$INSTDIR\uninstall.exe"
	WriteUninstaller "$INSTDIR\uninstall.exe"

SectionEnd

; Modern install component descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${Section1} ""
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;Uninstall section
Section Uninstall

	;Remove from registry...
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}"
        DeleteRegKey HKLM "SOFTWARE\${APPNAME}"

	; Delete self
	Delete "$INSTDIR\uninstall.exe"

	; Delete Shortcuts
	Delete "$SMPROGRAMS\Administrator\Uninstall.lnk"

	; Clean up Agent
	Delete "$INSTDIR\Administrator.exe"
	Delete "$INSTDIR\Interfaces.dll"
	Delete "$INSTDIR\Newtonsoft.Json.dll"
	Delete "Administrator\bin\Debug\ThoughtWorks.QRCode.dll"

	; Remove remaining directories
	RMDir "$SMPROGRAMS\Administrator"
	RMDir "$INSTDIR\"

SectionEnd

; eof