﻿namespace Administrator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComputerListBox = new System.Windows.Forms.ListBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.TabPanel = new System.Windows.Forms.TabControl();
            this.InformationTab = new System.Windows.Forms.TabPage();
            this.SaveImgBtn = new System.Windows.Forms.Button();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.BtnGenerateQR = new System.Windows.Forms.Button();
            this.QRImage = new System.Windows.Forms.PictureBox();
            this.InventoryBox = new System.Windows.Forms.TextBox();
            this.DomainBox = new System.Windows.Forms.TextBox();
            this.UserNameBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ComputerNameBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CommentBox = new System.Windows.Forms.TextBox();
            this.ConfigurationTab = new System.Windows.Forms.TabPage();
            this.VideoList = new System.Windows.Forms.ListView();
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NetworkAdapterList = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RAMList = new System.Windows.Forms.ListView();
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DisplayList = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CDROMList = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ProcessorList = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DiskDriveList = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MotherboardList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SoftwareTab = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.SoftwareListBox = new System.Windows.Forms.ListBox();
            this.HistoryTab = new System.Windows.Forms.TabPage();
            this.HistoryList = new System.Windows.Forms.ListView();
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ConsoleBox = new System.Windows.Forms.TextBox();
            this.ConnectBtn = new System.Windows.Forms.Button();
            this.DisconnectBtn = new System.Windows.Forms.Button();
            this.PasswordBox = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ServerBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.MainMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.TabPanel.SuspendLayout();
            this.InformationTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QRImage)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.ConfigurationTab.SuspendLayout();
            this.SoftwareTab.SuspendLayout();
            this.HistoryTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatusBar
            // 
            this.StatusBar.Location = new System.Drawing.Point(0, 405);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(676, 22);
            this.StatusBar.TabIndex = 0;
            this.StatusBar.Text = "statusStrip1";
            // 
            // MainMenu
            // 
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviceToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(162, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "menuStrip1";
            // 
            // serviceToolStripMenuItem
            // 
            this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
            this.serviceToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.serviceToolStripMenuItem.Text = "Service";
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComputerListBox);
            this.groupBox1.Controls.Add(this.SaveBtn);
            this.groupBox1.Controls.Add(this.DeleteBtn);
            this.groupBox1.Controls.Add(this.UpdateBtn);
            this.groupBox1.Location = new System.Drawing.Point(0, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 375);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List of computers";
            // 
            // ComputerListBox
            // 
            this.ComputerListBox.FormattingEnabled = true;
            this.ComputerListBox.Location = new System.Drawing.Point(6, 19);
            this.ComputerListBox.Name = "ComputerListBox";
            this.ComputerListBox.Size = new System.Drawing.Size(188, 264);
            this.ComputerListBox.TabIndex = 7;
            this.ComputerListBox.Click += new System.EventHandler(this.ComputerListBox_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.Enabled = false;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Location = new System.Drawing.Point(64, 317);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveBtn.TabIndex = 6;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Enabled = false;
            this.DeleteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteBtn.Location = new System.Drawing.Point(64, 346);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(75, 23);
            this.DeleteBtn.TabIndex = 5;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Enabled = false;
            this.UpdateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpdateBtn.Location = new System.Drawing.Point(64, 288);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(75, 23);
            this.UpdateBtn.TabIndex = 4;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateBtn_Click);
            // 
            // TabPanel
            // 
            this.TabPanel.Controls.Add(this.InformationTab);
            this.TabPanel.Controls.Add(this.ConfigurationTab);
            this.TabPanel.Controls.Add(this.SoftwareTab);
            this.TabPanel.Controls.Add(this.HistoryTab);
            this.TabPanel.Location = new System.Drawing.Point(206, 32);
            this.TabPanel.Name = "TabPanel";
            this.TabPanel.SelectedIndex = 0;
            this.TabPanel.Size = new System.Drawing.Size(458, 277);
            this.TabPanel.TabIndex = 4;
            // 
            // InformationTab
            // 
            this.InformationTab.Controls.Add(this.SaveImgBtn);
            this.InformationTab.Controls.Add(this.PrintBtn);
            this.InformationTab.Controls.Add(this.BtnGenerateQR);
            this.InformationTab.Controls.Add(this.QRImage);
            this.InformationTab.Controls.Add(this.InventoryBox);
            this.InformationTab.Controls.Add(this.DomainBox);
            this.InformationTab.Controls.Add(this.UserNameBox);
            this.InformationTab.Controls.Add(this.label4);
            this.InformationTab.Controls.Add(this.label3);
            this.InformationTab.Controls.Add(this.label2);
            this.InformationTab.Controls.Add(this.ComputerNameBox);
            this.InformationTab.Controls.Add(this.label1);
            this.InformationTab.Controls.Add(this.groupBox3);
            this.InformationTab.Location = new System.Drawing.Point(4, 22);
            this.InformationTab.Name = "InformationTab";
            this.InformationTab.Padding = new System.Windows.Forms.Padding(3);
            this.InformationTab.Size = new System.Drawing.Size(450, 251);
            this.InformationTab.TabIndex = 0;
            this.InformationTab.Text = "Information";
            this.InformationTab.UseVisualStyleBackColor = true;
            // 
            // SaveImgBtn
            // 
            this.SaveImgBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveImgBtn.Location = new System.Drawing.Point(321, 144);
            this.SaveImgBtn.Name = "SaveImgBtn";
            this.SaveImgBtn.Size = new System.Drawing.Size(66, 23);
            this.SaveImgBtn.TabIndex = 11;
            this.SaveImgBtn.Text = "Save Img";
            this.SaveImgBtn.UseVisualStyleBackColor = true;
            this.SaveImgBtn.Click += new System.EventHandler(this.SaveImgBtn_Click);
            // 
            // PrintBtn
            // 
            this.PrintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PrintBtn.Location = new System.Drawing.Point(393, 144);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(45, 23);
            this.PrintBtn.TabIndex = 7;
            this.PrintBtn.Text = "Print";
            this.PrintBtn.UseVisualStyleBackColor = true;
            // 
            // BtnGenerateQR
            // 
            this.BtnGenerateQR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGenerateQR.Location = new System.Drawing.Point(290, 112);
            this.BtnGenerateQR.Name = "BtnGenerateQR";
            this.BtnGenerateQR.Size = new System.Drawing.Size(26, 23);
            this.BtnGenerateQR.TabIndex = 10;
            this.BtnGenerateQR.Text = "->";
            this.BtnGenerateQR.UseVisualStyleBackColor = true;
            this.BtnGenerateQR.Click += new System.EventHandler(this.BtnGenerateQR_Click);
            // 
            // QRImage
            // 
            this.QRImage.BackColor = System.Drawing.Color.Transparent;
            this.QRImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.QRImage.Location = new System.Drawing.Point(321, 29);
            this.QRImage.Name = "QRImage";
            this.QRImage.Size = new System.Drawing.Size(117, 105);
            this.QRImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.QRImage.TabIndex = 9;
            this.QRImage.TabStop = false;
            // 
            // InventoryBox
            // 
            this.InventoryBox.Location = new System.Drawing.Point(127, 114);
            this.InventoryBox.Name = "InventoryBox";
            this.InventoryBox.Size = new System.Drawing.Size(157, 20);
            this.InventoryBox.TabIndex = 8;
            // 
            // DomainBox
            // 
            this.DomainBox.Enabled = false;
            this.DomainBox.Location = new System.Drawing.Point(127, 86);
            this.DomainBox.Name = "DomainBox";
            this.DomainBox.Size = new System.Drawing.Size(157, 20);
            this.DomainBox.TabIndex = 7;
            this.DomainBox.Text = "In developing...";
            // 
            // UserNameBox
            // 
            this.UserNameBox.Enabled = false;
            this.UserNameBox.Location = new System.Drawing.Point(127, 59);
            this.UserNameBox.Name = "UserNameBox";
            this.UserNameBox.Size = new System.Drawing.Size(157, 20);
            this.UserNameBox.TabIndex = 6;
            this.UserNameBox.Text = "In developing...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Inventory number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Domain/Workstation:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "User name:";
            // 
            // ComputerNameBox
            // 
            this.ComputerNameBox.Location = new System.Drawing.Point(127, 30);
            this.ComputerNameBox.Name = "ComputerNameBox";
            this.ComputerNameBox.Size = new System.Drawing.Size(157, 20);
            this.ComputerNameBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Computer name:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CommentBox);
            this.groupBox3.Location = new System.Drawing.Point(7, 173);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(437, 73);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Comment";
            // 
            // CommentBox
            // 
            this.CommentBox.Location = new System.Drawing.Point(6, 19);
            this.CommentBox.Multiline = true;
            this.CommentBox.Name = "CommentBox";
            this.CommentBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CommentBox.Size = new System.Drawing.Size(425, 49);
            this.CommentBox.TabIndex = 0;
            // 
            // ConfigurationTab
            // 
            this.ConfigurationTab.AutoScroll = true;
            this.ConfigurationTab.Controls.Add(this.label15);
            this.ConfigurationTab.Controls.Add(this.label14);
            this.ConfigurationTab.Controls.Add(this.label13);
            this.ConfigurationTab.Controls.Add(this.label12);
            this.ConfigurationTab.Controls.Add(this.label11);
            this.ConfigurationTab.Controls.Add(this.label10);
            this.ConfigurationTab.Controls.Add(this.label9);
            this.ConfigurationTab.Controls.Add(this.label8);
            this.ConfigurationTab.Controls.Add(this.VideoList);
            this.ConfigurationTab.Controls.Add(this.NetworkAdapterList);
            this.ConfigurationTab.Controls.Add(this.RAMList);
            this.ConfigurationTab.Controls.Add(this.DisplayList);
            this.ConfigurationTab.Controls.Add(this.CDROMList);
            this.ConfigurationTab.Controls.Add(this.ProcessorList);
            this.ConfigurationTab.Controls.Add(this.DiskDriveList);
            this.ConfigurationTab.Controls.Add(this.MotherboardList);
            this.ConfigurationTab.Location = new System.Drawing.Point(4, 22);
            this.ConfigurationTab.Name = "ConfigurationTab";
            this.ConfigurationTab.Padding = new System.Windows.Forms.Padding(3);
            this.ConfigurationTab.Size = new System.Drawing.Size(450, 251);
            this.ConfigurationTab.TabIndex = 1;
            this.ConfigurationTab.Text = "Configuration";
            this.ConfigurationTab.UseVisualStyleBackColor = true;
            // 
            // VideoList
            // 
            this.VideoList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17});
            this.VideoList.Location = new System.Drawing.Point(25, 602);
            this.VideoList.Name = "VideoList";
            this.VideoList.Size = new System.Drawing.Size(365, 77);
            this.VideoList.TabIndex = 7;
            this.VideoList.UseCompatibleStateImageBehavior = false;
            this.VideoList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Name";
            this.columnHeader15.Width = 168;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Vendor_ID";
            this.columnHeader16.Width = 117;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Device_ID";
            this.columnHeader17.Width = 107;
            // 
            // NetworkAdapterList
            // 
            this.NetworkAdapterList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader18,
            this.columnHeader19});
            this.NetworkAdapterList.Location = new System.Drawing.Point(25, 700);
            this.NetworkAdapterList.Name = "NetworkAdapterList";
            this.NetworkAdapterList.Size = new System.Drawing.Size(365, 77);
            this.NetworkAdapterList.TabIndex = 3;
            this.NetworkAdapterList.UseCompatibleStateImageBehavior = false;
            this.NetworkAdapterList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Name";
            this.columnHeader7.Width = 168;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Vendor_ID";
            this.columnHeader8.Width = 83;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Device_ID";
            this.columnHeader18.Width = 82;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "MAC";
            this.columnHeader19.Width = 108;
            // 
            // RAMList
            // 
            this.RAMList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader11});
            this.RAMList.Location = new System.Drawing.Point(25, 505);
            this.RAMList.Name = "RAMList";
            this.RAMList.Size = new System.Drawing.Size(365, 77);
            this.RAMList.TabIndex = 6;
            this.RAMList.UseCompatibleStateImageBehavior = false;
            this.RAMList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Slot";
            this.columnHeader13.Width = 109;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Size";
            this.columnHeader14.Width = 121;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Serial";
            this.columnHeader11.Width = 130;
            // 
            // DisplayList
            // 
            this.DisplayList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12});
            this.DisplayList.Location = new System.Drawing.Point(25, 408);
            this.DisplayList.Name = "DisplayList";
            this.DisplayList.Size = new System.Drawing.Size(365, 77);
            this.DisplayList.TabIndex = 5;
            this.DisplayList.UseCompatibleStateImageBehavior = false;
            this.DisplayList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "ID";
            this.columnHeader12.Width = 190;
            // 
            // CDROMList
            // 
            this.CDROMList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10});
            this.CDROMList.Location = new System.Drawing.Point(25, 313);
            this.CDROMList.Name = "CDROMList";
            this.CDROMList.Size = new System.Drawing.Size(365, 77);
            this.CDROMList.TabIndex = 4;
            this.CDROMList.UseCompatibleStateImageBehavior = false;
            this.CDROMList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Name";
            this.columnHeader9.Width = 168;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Serial";
            this.columnHeader10.Width = 190;
            // 
            // ProcessorList
            // 
            this.ProcessorList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.ProcessorList.Location = new System.Drawing.Point(25, 215);
            this.ProcessorList.Name = "ProcessorList";
            this.ProcessorList.Size = new System.Drawing.Size(365, 77);
            this.ProcessorList.TabIndex = 2;
            this.ProcessorList.UseCompatibleStateImageBehavior = false;
            this.ProcessorList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Name";
            this.columnHeader5.Width = 168;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "ID";
            this.columnHeader6.Width = 190;
            // 
            // DiskDriveList
            // 
            this.DiskDriveList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.DiskDriveList.Location = new System.Drawing.Point(25, 119);
            this.DiskDriveList.Name = "DiskDriveList";
            this.DiskDriveList.Size = new System.Drawing.Size(365, 77);
            this.DiskDriveList.TabIndex = 1;
            this.DiskDriveList.UseCompatibleStateImageBehavior = false;
            this.DiskDriveList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 168;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Serial";
            this.columnHeader4.Width = 190;
            // 
            // MotherboardList
            // 
            this.MotherboardList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.MotherboardList.Location = new System.Drawing.Point(25, 23);
            this.MotherboardList.Name = "MotherboardList";
            this.MotherboardList.Size = new System.Drawing.Size(365, 77);
            this.MotherboardList.TabIndex = 0;
            this.MotherboardList.UseCompatibleStateImageBehavior = false;
            this.MotherboardList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Manufacture";
            this.columnHeader1.Width = 168;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Model";
            this.columnHeader2.Width = 190;
            // 
            // SoftwareTab
            // 
            this.SoftwareTab.Controls.Add(this.label5);
            this.SoftwareTab.Controls.Add(this.SoftwareListBox);
            this.SoftwareTab.Location = new System.Drawing.Point(4, 22);
            this.SoftwareTab.Name = "SoftwareTab";
            this.SoftwareTab.Padding = new System.Windows.Forms.Padding(3);
            this.SoftwareTab.Size = new System.Drawing.Size(450, 251);
            this.SoftwareTab.TabIndex = 2;
            this.SoftwareTab.Text = "Software";
            this.SoftwareTab.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "List of Software:";
            // 
            // SoftwareListBox
            // 
            this.SoftwareListBox.FormattingEnabled = true;
            this.SoftwareListBox.Location = new System.Drawing.Point(32, 41);
            this.SoftwareListBox.Name = "SoftwareListBox";
            this.SoftwareListBox.ScrollAlwaysVisible = true;
            this.SoftwareListBox.Size = new System.Drawing.Size(386, 199);
            this.SoftwareListBox.TabIndex = 0;
            // 
            // HistoryTab
            // 
            this.HistoryTab.Controls.Add(this.HistoryList);
            this.HistoryTab.Location = new System.Drawing.Point(4, 22);
            this.HistoryTab.Name = "HistoryTab";
            this.HistoryTab.Padding = new System.Windows.Forms.Padding(3);
            this.HistoryTab.Size = new System.Drawing.Size(450, 251);
            this.HistoryTab.TabIndex = 4;
            this.HistoryTab.Text = "History";
            this.HistoryTab.UseVisualStyleBackColor = true;
            // 
            // HistoryList
            // 
            this.HistoryList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23});
            this.HistoryList.Location = new System.Drawing.Point(32, 6);
            this.HistoryList.Name = "HistoryList";
            this.HistoryList.Size = new System.Drawing.Size(382, 239);
            this.HistoryList.TabIndex = 0;
            this.HistoryList.UseCompatibleStateImageBehavior = false;
            this.HistoryList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Date";
            this.columnHeader20.Width = 81;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Event";
            this.columnHeader21.Width = 84;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Description";
            this.columnHeader22.Width = 152;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Comment";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ConsoleBox);
            this.groupBox2.Location = new System.Drawing.Point(210, 315);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(454, 87);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "System status";
            // 
            // ConsoleBox
            // 
            this.ConsoleBox.Location = new System.Drawing.Point(7, 20);
            this.ConsoleBox.Multiline = true;
            this.ConsoleBox.Name = "ConsoleBox";
            this.ConsoleBox.ReadOnly = true;
            this.ConsoleBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ConsoleBox.Size = new System.Drawing.Size(441, 61);
            this.ConsoleBox.TabIndex = 0;
            this.ConsoleBox.Text = "Administrator application is running";
            // 
            // ConnectBtn
            // 
            this.ConnectBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConnectBtn.Location = new System.Drawing.Point(508, 3);
            this.ConnectBtn.Name = "ConnectBtn";
            this.ConnectBtn.Size = new System.Drawing.Size(75, 23);
            this.ConnectBtn.TabIndex = 6;
            this.ConnectBtn.Text = "Connect";
            this.ConnectBtn.UseVisualStyleBackColor = true;
            this.ConnectBtn.Click += new System.EventHandler(this.ConnectBtn_Click);
            // 
            // DisconnectBtn
            // 
            this.DisconnectBtn.Enabled = false;
            this.DisconnectBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DisconnectBtn.Location = new System.Drawing.Point(589, 3);
            this.DisconnectBtn.Name = "DisconnectBtn";
            this.DisconnectBtn.Size = new System.Drawing.Size(75, 23);
            this.DisconnectBtn.TabIndex = 7;
            this.DisconnectBtn.Text = "Disconnect";
            this.DisconnectBtn.UseVisualStyleBackColor = true;
            this.DisconnectBtn.Click += new System.EventHandler(this.DisconnectBtn_Click);
            // 
            // PasswordBox
            // 
            this.PasswordBox.Location = new System.Drawing.Point(402, 5);
            this.PasswordBox.Name = "PasswordBox";
            this.PasswordBox.Size = new System.Drawing.Size(100, 20);
            this.PasswordBox.TabIndex = 8;
            this.PasswordBox.Text = "test_admin";
            this.PasswordBox.UseSystemPasswordChar = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(340, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Password:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(187, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Server:";
            // 
            // ServerBox
            // 
            this.ServerBox.Location = new System.Drawing.Point(234, 5);
            this.ServerBox.Name = "ServerBox";
            this.ServerBox.Size = new System.Drawing.Size(100, 20);
            this.ServerBox.TabIndex = 12;
            this.ServerBox.Text = "192.168.0.107";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Motherboard:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Disk Drive:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Processor:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 297);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "CD-ROM:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 393);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Display:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(25, 489);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "RAM:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 586);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Video:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 684);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "Network Adapter:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 427);
            this.Controls.Add(this.ServerBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.PasswordBox);
            this.Controls.Add(this.DisconnectBtn);
            this.Controls.Add(this.ConnectBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.TabPanel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.MainMenu;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Administrator";
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.TabPanel.ResumeLayout(false);
            this.InformationTab.ResumeLayout(false);
            this.InformationTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QRImage)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ConfigurationTab.ResumeLayout(false);
            this.ConfigurationTab.PerformLayout();
            this.SoftwareTab.ResumeLayout(false);
            this.SoftwareTab.PerformLayout();
            this.HistoryTab.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TabControl TabPanel;
        private System.Windows.Forms.TabPage InformationTab;
        private System.Windows.Forms.TabPage ConfigurationTab;
        private System.Windows.Forms.TabPage SoftwareTab;
        private System.Windows.Forms.TabPage HistoryTab;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox ConsoleBox;
        private System.Windows.Forms.TextBox InventoryBox;
        private System.Windows.Forms.TextBox DomainBox;
        private System.Windows.Forms.TextBox UserNameBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ComputerNameBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox CommentBox;
        private System.Windows.Forms.ListView DiskDriveList;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ListView MotherboardList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView VideoList;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ListView RAMList;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ListView DisplayList;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ListView CDROMList;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ListView NetworkAdapterList;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ListView ProcessorList;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ListBox SoftwareListBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView HistoryList;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.Button BtnGenerateQR;
        private System.Windows.Forms.PictureBox QRImage;
        private System.Windows.Forms.Button ConnectBtn;
        private System.Windows.Forms.Button DisconnectBtn;
        private System.Windows.Forms.MaskedTextBox PasswordBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ServerBox;
        private System.Windows.Forms.Button SaveImgBtn;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.ListBox ComputerListBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}

