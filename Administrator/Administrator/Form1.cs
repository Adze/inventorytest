﻿using Client;
using Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMI_NS;

namespace Administrator
{
    public partial class Form1 : Form
    {
        Client_class client = new Client_class();

        public Form1()
        {
            InitializeComponent();
            Errors.ErrorEventHandler = new Errors.ErrorEvent(OutputEvents);
        }

        void OutputEvents(string Msg)
        {
            ConsoleBox.Invoke(new Action(() =>
                {
                    ConsoleBox.Text += Environment.NewLine;
                    ConsoleBox.Text += Msg;

                }));
            //ConsoleBox.Text += Environment.NewLine;
            //ConsoleBox.Text += Msg;
        }

        private void BtnGenerateQR_Click(object sender, EventArgs e)
        {
            QRImage.Image = new QR_Class().GenereateQR(InventoryBox.Text);
        }

        private void EnableControl(bool action)
        {
            //Button control
            ConnectBtn.Enabled = !action;
            DisconnectBtn.Enabled = action;
            UpdateBtn.Enabled = action;
            DeleteBtn.Enabled = action;
            SaveBtn.Enabled = action;

            //TextBox control
            ServerBox.Enabled = !action;
            PasswordBox.Enabled = !action;
        }

        private void GetComputerList()
        {
            List<string> ComputerList = client.Admin.GetComputerList();

            foreach (string str in ComputerList)
            {
                ComputerListBox.Items.Clear();
                ComputerListBox.Items.Add(str);
            }
        }

        private void ShowData(Computer Comp, List<History> history)
        {
            int index;

            ComputerNameBox.Text = Comp._ComputerName.Name;
            InventoryBox.Text = Comp._Inventory;
            CommentBox.Text = Comp._Comment;

            MotherboardList.Items.Add(Comp._Motherboard.Manufacturer);
            MotherboardList.Items[0].SubItems.Add(Comp._Motherboard.Name);

            for(int i=0;i<Comp._DiskDrive.Length;i++)
            {
                index = DiskDriveList.Items.Add(Comp._DiskDrive[i].Name).Index;
                DiskDriveList.Items[index].SubItems.Add(Comp._DiskDrive[i].Serial);
            }

            ProcessorList.Items.Add(Comp._Processor.Name);
            ProcessorList.Items[0].SubItems.Add(Comp._Processor.ID);

            for (int i = 0; i < Comp._CDROMDrive.Length; i++)
            {
                index = CDROMList.Items.Add(Comp._CDROMDrive[i].Name).Index;
                CDROMList.Items[index].SubItems.Add(Comp._CDROMDrive[i].Serial);
            }

            for (int i = 0; i < Comp._Display.Length; i++)
            {
                DisplayList.Items.Add(Comp._Display[i].ID);
            }

            for (int i = 0; i < Comp._RAM.Length; i++)
            {
                index = RAMList.Items.Add(Comp._RAM[i].Slot).Index;
                RAMList.Items[index].SubItems.Add(Comp._RAM[i].Size);
                RAMList.Items[index].SubItems.Add(Comp._RAM[i].Serial);
            }

            for (int i = 0; i < Comp._Video.Length; i++)
            {
                index = VideoList.Items.Add(Comp._Video[i].Name).Index;
                VideoList.Items[index].SubItems.Add(Comp._Video[i].VEN_ID);
                VideoList.Items[index].SubItems.Add(Comp._Video[i].DEV_ID);
            }

            for (int i = 0; i < Comp._NetworkAdapter.Length; i++)
            {
                index = NetworkAdapterList.Items.Add(Comp._NetworkAdapter[i].Name).Index;
                NetworkAdapterList.Items[index].SubItems.Add(Comp._NetworkAdapter[i].VEN_ID);
                NetworkAdapterList.Items[index].SubItems.Add(Comp._NetworkAdapter[i].DEV_ID);
                NetworkAdapterList.Items[index].SubItems.Add(Comp._NetworkAdapter[i].MAC);
            }

            for (int i = 0; i < Comp._Software.Length; i++)
            {
                SoftwareListBox.Items.Add(Comp._Software[i].Name);
            }

            foreach(History h in history)
            {
                index = HistoryList.Items.Add(h.Date).Index;
                HistoryList.Items[index].SubItems.Add(h.Event);
                HistoryList.Items[index].SubItems.Add(h.Description);
            }
        }

        private void ClearForm()
        {
            ComputerNameBox.Clear();
            ComputerListBox.Items.Clear();
            InventoryBox.Clear();
            CommentBox.Clear();
            QRImage.Image = null;
            MotherboardList.Clear();
            DiskDriveList.Clear();
            ProcessorList.Clear();
            CDROMList.Clear();
            DisplayList.Clear();
            RAMList.Clear();
            VideoList.Clear();
            NetworkAdapterList.Clear();
            SoftwareListBox.Items.Clear();
            HistoryList.Clear();
        }

        private void ConnectBtn_Click(object sender, EventArgs e)
        {
            client.Host = ServerBox.Text;
            client.Port = 2000;
            client.Events.OnError = OnError;
            try
            {
                if (client.Connect(true))
                {
                    client.Common.ChangePrivileges("Admin", PasswordBox.Text);
                    EnableControl(true);
                    GetComputerList();
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler(GetAllNestedMessages(ex));
            }
        }

        private static string GetAllNestedMessages(Exception ex)
        {
            string s = ex.Message;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                s += string.Concat(Environment.NewLine, ex.Message);
            }
            return s;
        }

        static void OnError(Exception ex)
        {
            Console.WriteLine(GetAllNestedMessages(ex));
            Errors.ErrorEventHandler(GetAllNestedMessages(ex));
        }

        private void DisconnectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (client.Disconnect(true))
                {
                    EnableControl(false);
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler(GetAllNestedMessages(ex));
            }
        }

        private void ComputerListBox_Click(object sender, EventArgs e)
        {
            ComputerNameBox.Text = ComputerListBox.SelectedItem.ToString();
            string json = client.Admin.GetComputer(ComputerListBox.SelectedItem.ToString());
            Computer Comp = Newtonsoft.Json.JsonConvert.DeserializeObject<Computer>(json);
            json = client.Admin.GetHistory(Comp._ComputerName.Name);
            List<History> history = Newtonsoft.Json.JsonConvert.DeserializeObject<List<History>>(json);
            ShowData(Comp, history);
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            ClearForm();
            GetComputerList();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            client.Admin.SaveChanges(ComputerNameBox.Text, InventoryBox.Text, CommentBox.Text);
        }

        private void SaveImgBtn_Click(object sender, EventArgs e)
        {
            Image img = QRImage.Image;
            SaveFileDialog SaveDialog = new SaveFileDialog();
            SaveDialog.Filter = "PNG Image|*.png";
            SaveDialog.Title = "Save QR Code Image";
            SaveDialog.FileName = ComputerNameBox.Text;
            if (SaveDialog.ShowDialog() == DialogResult.OK)
            {
                if (SaveDialog.FileName != "")
                {
                    img.Save(SaveDialog.FileName, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            client.Admin.DeleteComputer(ComputerNameBox.Text);
            ClearForm();
            GetComputerList();
        }
    }
}
