﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrator
{
    public static class Logs
    {
        public delegate void LogEvent(string LogMsg);

        public static LogEvent LogEventHandler;
    }

    public static class Errors
    {
        public delegate void ErrorEvent(string ErrorMsg);

        public static ErrorEvent ErrorEventHandler;
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
