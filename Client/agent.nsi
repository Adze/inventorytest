; Script generated with the Venis Install Wizard

; Define your application name
!define APPNAME "Agent"
!define APPNAMEANDVERSION "Agent 1.0"

; Main Install settings
Name "${APPNAMEANDVERSION}"
InstallDir "$PROGRAMFILES\Agent"
InstallDirRegKey HKLM "Software\${APPNAME}" ""
OutFile "Agent_Setup.exe"

; Modern interface settings
!include "MUI.nsh"

!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_WELCOME
page custom SetParams GetParams
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH


!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Set languages (first is default language)
!insertmacro MUI_LANGUAGE "Russian"
!insertmacro MUI_RESERVEFILE_LANGDLL

Var "Server"
Var "Port"
Var "User"
Var "Password"
Var "Timer"

Function .onInit
   initpluginsdir
   file /oname=$PLUGINSDIR\custom.ini custom.ini
   Push $0
FunctionEnd

Function GetParams
!insertmacro MUI_INSTALLOPTIONS_READ $Server "custom.ini" "Field 8" "State"
!insertmacro MUI_INSTALLOPTIONS_READ $Port "custom.ini" "Field 6" "State"
!insertmacro MUI_INSTALLOPTIONS_READ $User "custom.ini" "Field 7" "State"
!insertmacro MUI_INSTALLOPTIONS_READ $Password "custom.ini" "Field 9" "State"
FunctionEnd

Function SetParams
   !insertmacro MUI_HEADER_TEXT "��������� ����������" "������� ����������� ������"
   Push $1 ;park value to the stack
   InstallOptions::initDialog /NOUNLOAD '$PLUGINSDIR\custom.ini'
   Pop $1 ;get dialog HWND
   InstallOptions::show
   Pop $1 ;get button action
   Pop $1 ;get custom.ini full path
   Pop $1 ;get back value from stack
FunctionEnd

Section "Agent" Section1

	; Set Section properties
	SetOverwrite on

	; Set Section Files and Shortcuts
	SetOutPath "$INSTDIR\"
	File "Client\bin\Debug\Client.exe"
	File "Client\bin\Debug\Interfaces.dll"
	File "Client\bin\Debug\Newtonsoft.Json.dll"
	CreateDirectory "$SMPROGRAMS\Agent"
	CreateShortCut "$SMPROGRAMS\Agent\Uninstall.lnk" "$INSTDIR\uninstall.exe"

SectionEnd

Section -FinishSection

        ExecWait "C:\Windows\Microsoft.NET\Framework\v4.0.30319\installutil.exe $INSTDIR\Agent.exe"
	WriteRegStr HKLM "Software\${APPNAME}" "" "$INSTDIR"
	WriteRegStr HKLM "Software\${APPNAME}" "Server" "$Server"
	WriteRegStr HKLM "Software\${APPNAME}" "Port" "$Port"
	WriteRegStr HKLM "Software\${APPNAME}" "User" "$User"
	WriteRegStr HKLM "Software\${APPNAME}" "Password" "$Password"
        WriteRegStr HKLM "Software\${APPNAME}" "Timer" "$Timer"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "DisplayName" "${APPNAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "UninstallString" "$INSTDIR\uninstall.exe"
	WriteUninstaller "$INSTDIR\uninstall.exe"

SectionEnd

; Modern install component descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${Section1} ""
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;Uninstall section
Section Uninstall

        ExecWait "sc delete Agent"
	;Remove from registry...
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}"
        DeleteRegKey HKLM "SOFTWARE\${APPNAME}"

	; Delete self
	Delete "$INSTDIR\uninstall.exe"

	; Delete Shortcuts
	Delete "$SMPROGRAMS\Agent\Uninstall.lnk"

	; Clean up Agent
	Delete "$INSTDIR\Client.exe"
	Delete "$INSTDIR\Interfaces.dll"
	Delete "$INSTDIR\Newtonsoft.Json.dll"

	; Remove remaining directories
	RMDir "$SMPROGRAMS\Agent"
	RMDir "$INSTDIR\"
	
	MessageBox MB_YESNO|MB_ICONQUESTION "Do you wish to reboot the system?" IDNO +2
                   Reboot

SectionEnd

; eof