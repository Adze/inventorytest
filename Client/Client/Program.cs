﻿using System;
using System.Collections.Generic;
using Log;
using System.Diagnostics;
using WMI_NS;
using Newtonsoft;

namespace Client
{
    public static class Errors
    {
        public delegate void ErrorEvent(string ErrorMsg);

        public static ErrorEvent ErrorEventHandler;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Log.Log LogFile = new Log.Log();
            WMI _wmi = new WMI();
            string test = string.Empty;

            /*Stopwatch sw = new Stopwatch();

            sw.Start();
            Console.WriteLine("------------Disk Drive------------\n");
            foreach (DiskDrive dd in _wmi.GetDiskDrive())
            {
                Console.WriteLine("Name: " + dd.Name + "\nSerial: " + dd.Serial);
                test += dd.Name + dd.Serial;
            }

            Console.WriteLine("\n------------Processor------------\n");
            Processor proc = _wmi.GetProcessor();
            Console.WriteLine("Name: " + proc.Name + "\nID: " + proc.ID);
            test += proc.Name + proc.ID;

            Console.WriteLine("\n------------CDROM------------\n");
            foreach (CDROMDrive dd in _wmi.GetCDROM())
            {
                Console.WriteLine("Name: " + dd.Name + "\nSerial: " + dd.Serial);
                test += dd.Name + dd.Serial;
            }

            Console.WriteLine("\n------------Computer Name------------\n");
            ComputerName cname = _wmi.GetComputerName();
            Console.WriteLine("Name: " + cname.Name);
            test += cname.Name;

            Console.WriteLine("\n------------Motherboard------------\n");
            Motherboard mb = _wmi.GetMotherboard();
            Console.WriteLine("Name: " + mb.Name + "\nManufacturer: " + mb.Manufacturer);
            test += mb.Name + mb.Manufacturer;

            Console.WriteLine("\n------------Monitor------------\n");
            foreach (Display dd in _wmi.GetMonitor())
            {
                Console.WriteLine("ID: " + dd.ID);
                test += dd.ID;
            }

            Console.WriteLine("\n------------Network Adapters------------\n");
            foreach (NetworkAdapter dd in _wmi.GetNetworkAdapter())
            {
                Console.WriteLine("Name: " + dd.Name + "\nMAC: " + dd.MAC + "\nVendor_ID = " + dd.VEN_ID + "\nDeviceID = " + dd.DEV_ID);
                test += dd.Name + dd.MAC + dd.VEN_ID + dd.DEV_ID;
            }

            Console.WriteLine("\n------------Video------------\n");
            foreach (Video dd in _wmi.GetVideo())
            {
                Console.WriteLine("Name: " + dd.Name + "\nVendor_ID = " + dd.VEN_ID + "\nDeviceID = " + dd.DEV_ID);
                test += dd.Name + dd.VEN_ID + dd.DEV_ID;
            }

            Console.WriteLine("\n------------RAM------------\n");
            foreach (RAM dd in _wmi.GetRAM())
            {
                Console.WriteLine("Name: " + dd.Slot + "\nSize = " + float.Parse(dd.Size)/1024/1024 + " MB" + "\nSerial = " + dd.Serial);
                test += dd.Slot + dd.Size + dd.Serial;
            }

            foreach (Software dd in _wmi.GetSoftware())
            {
                test += dd.Name;
            }

            sw.Stop();

            Console.WriteLine("\n\n\n###Данные тестирования###");
            Console.WriteLine("Кол-во символов, полученных от WMI " + test.Length);
            TimeSpan ts = sw.Elapsed;
            Console.WriteLine("Время, затраченное на получение данных = {0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Нажмите клавишу для тестовой отправки данных...");
            Console.ReadKey();
            */
        //}

            Console.Title = "Клиент";
            Client_class client = new Client_class();
            client.Host = "192.168.0.107";
            client.Port = 2000;
            client.Events.OnError = OnError;
            client.Events.OnBark = OnBark;

            Console.WriteLine("Подключение...");
            if (client.Connect(false))
            {
                Console.WriteLine("Подключено!");
                string[] users = client.Common.GetAvailableUsers();

                client.Common.ChangePrivileges("Agent", "test_agent");

                Console.WriteLine("Делаем запрос к WMI");

                Computer Comp = _wmi.PrepareData();
                string WMI_Serializer = Newtonsoft.Json.JsonConvert.SerializeObject(Comp, Newtonsoft.Json.Formatting.Indented);
                Console.WriteLine("Формируем данные для сервера...");
                /*string[] WMI_Serializer = new string[array.Count];
                int i = 0;
                foreach (RAM dd in _wmi.GetRAM())
                {
                    WMI_Serializer[i] = Newtonsoft.Json.JsonConvert.SerializeObject(dd, Newtonsoft.Json.Formatting.Indented);
                    i++;
                }*/
                //string WMI_Serializer = Newtonsoft.Json.JsonConvert.SerializeObject(DD, Newtonsoft.Json.Formatting.Indented);
                Console.WriteLine("Отправляем данные серверу...");
                client.Agent.Transfer_Comp(WMI_Serializer);
                //client.Agent.Transfer_DiskDrive(WMI_Serializer);
                Console.WriteLine("Готово!");

                /*string s = "qwerty";
                try
                {
                    Console.WriteLine("<- CutTheText(ref {0})", s);
                    client.Cat.CutTheText(ref s);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Concat("-> \"", GetAllNestedMessages(ex), "\""));
                }

                try
                {
                    Console.WriteLine("<- ChangePrivileges(Tom, _password)");
                    client.Common.ChangePrivileges("Tom", "_password");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Concat("-> \"", GetAllNestedMessages(ex), "\""));
                }

                Console.WriteLine("<- ChangePrivileges(Tom, TheCat)");
                client.Common.ChangePrivileges("Tom", "TheCat");

                Console.WriteLine("<- CutTheText(ref {0})", s);
                client.Cat.CutTheText(ref s);
                Console.WriteLine(@"-> ""{0}""", s);

                object o;
                try
                {
                    Console.WriteLine("<- TryFindObject");
                    client.Dog.TryFindObject(out o);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Concat("-> \"", GetAllNestedMessages(ex), "\""));
                }

                Console.WriteLine("<- ChangePrivileges(Dog1, _password)");
                client.Common.ChangePrivileges("Dog1", "_password");

                Console.WriteLine("<- TryFindObject");
                if (client.Dog.TryFindObject(out o))
                {
                    Console.WriteLine(@"-> Объект найден: ""{0}""", o);
                }

                try
                {
                    Console.WriteLine("<- CutTheText({0})", s);
                    client.Cat.CutTheText(ref s);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Concat("-> \"", GetAllNestedMessages(ex), "\""));
                }

                Console.WriteLine("<- ChangePrivileges(Dog0, groovy!)");
                client.Common.ChangePrivileges("Dog0", "groovy!");

                Console.WriteLine("<- Bark(1)");
                client.Dog.Bark(1);
            */
            }


            Console.WriteLine("Press any key to close");
            Console.ReadKey();
        }

        private static string GetAllNestedMessages(Exception ex)
        {
            string s = ex.Message;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                s += string.Concat(Environment.NewLine, ex.Message);
            }
            return s;
        }

        static void OnError(Exception ex)
        {
            Console.WriteLine(GetAllNestedMessages(ex));
        }

        static void OnBark(int nTimes)
        {
            for (int i = 0; i < nTimes; i++)
            {
                Console.WriteLine("-> Bark!");
            }
        }
    }
}
