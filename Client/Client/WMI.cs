﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Management;
using System.Management.Instrumentation;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Client;

namespace WMI_NS
{
    class WMI
    {
        public WMI()
        {
        }

        /// <summary>Парсинг строки на наличие Vendor_ID и Device_ID
        /// <returns>
        /// <para>Возвращает GroupCollection.</para> 
        /// <para>В [1] содержится Vendor_ID</para> 
        /// <para>В [2] содержится Device_ID</para>
        /// </returns>
        /// </summary>
        private GroupCollection GetVenDev(string str)
        {
            Regex rx = new Regex(@"\bVEN_(.{4})&DEV_(.{4})\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            Match match = rx.Match(str);
            GroupCollection group = match.Groups;
            
            return group;
        }

        /// <summary>Получить данные по HDD
        /// <para><returns>Возвращает List&lt;DiskDrive&gt;</returns></para>
        /// </summary>
        public List<DiskDrive> GetDiskDrive()
        {
            List<DiskDrive> result = new List<DiskDrive>();
            string Query = "SELECT * FROM Win32_DiskDrive";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Add(new DiskDrive(obj["Caption"].ToString().Trim(), (obj["SerialNumber"] != null) ? obj["SerialNumber"].ToString().Trim() : "N/A"));
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>Даные по процессору
        /// <para><returns>Возвращает Processor</returns></para>
        /// </summary>
        public Processor GetProcessor()
        {
            Processor result = new Processor();
            string Query = "SELECT * FROM Win32_Processor";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Name = obj["Name"].ToString().Trim();
                    result.ID = obj["ProcessorID"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>CD-ROM
        /// <para><returns>Возвращает List&lt;CDROMDrive&gt;</returns></para>
        /// </summary>
        public List<CDROMDrive> GetCDROM()
        {
            List<CDROMDrive> result = new List<CDROMDrive>();
            string Query = "SELECT * FROM Win32_CDROMDrive";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Add(new CDROMDrive(obj["Caption"].ToString().Trim(), (obj["SerialNumber"] != null) ? obj["SerialNumber"].ToString().Trim() : "N/A"));
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>ComputerName
        /// <para><returns>Возвращает ComputerName</returns></para>
        /// </summary>
        public ComputerName GetComputerName()
        {
            ComputerName result = new ComputerName();
            string Query = "SELECT * FROM Win32_ComputerSystem";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Name = obj["Name"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>Motherboard
        /// <para><returns>Возвращает Motherboard</returns></para>
        /// </summary>
        public Motherboard GetMotherboard()
        {
            Motherboard result = new Motherboard();
            string Query = "SELECT * FROM Win32_BaseBoard";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Manufacturer = obj["Manufacturer"].ToString().Trim();
                    result.Name = obj["Product"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>Monitor
        /// <para><returns>Возвращает List&lt;Monitor&gt;</returns></para>
        /// </summary>
        public List<Display> GetMonitor()
        {
            List<Display> result = new List<Display>();
            string Query = "SELECT * FROM Win32_DesktopMonitor";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Add(new Display(obj["PNPDeviceID"].ToString().Trim()));
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>RAM
        /// <para><returns>Возвращает List&lt;RAM&gt;</returns></para>
        /// </summary>
        public List<RAM> GetRAM()
        {
            List<RAM> result = new List<RAM>();
            string Query = "SELECT * FROM Win32_PhysicalMemory";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Add(new RAM(obj["DeviceLocator"].ToString().Trim(), obj["Capacity"].ToString().Trim(), (obj["SerialNumber"] != null) ? obj["SerialNumber"].ToString().Trim() : "N/A"));
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>Video
        /// <para><returns>Возвращает List&lt;Video&gt;</returns></para>
        /// </summary>
        public List<Video> GetVideo()
        {
            List<Video> result = new List<Video>();
            string Query = "SELECT * FROM Win32_VideoController";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    GroupCollection group = GetVenDev(obj["PNPDeviceID"].ToString().Trim());
                    if (!string.IsNullOrEmpty(group[1].Value) & !string.IsNullOrEmpty(group[2].Value))
                    {
                        result.Add(new Video(obj["Name"].ToString().Trim(), group[1].Value, group[2].Value));
                    }
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>NetworkAdapter
        /// <para><returns>Возвращает List&lt;NetworkAdapter&gt;</returns></para>
        /// </summary>
        public List<NetworkAdapter> GetNetworkAdapter()
        {
            List<NetworkAdapter> result = new List<NetworkAdapter>();
            string Query = "SELECT * FROM Win32_NetworkAdapter";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    GroupCollection group = GetVenDev(obj["PNPDeviceID"].ToString().Trim());
                    if ((!string.IsNullOrEmpty(group[1].Value) & !string.IsNullOrEmpty(group[2].Value)) & (obj["MACAddress"] != null))
                    {
                        result.Add(new NetworkAdapter(obj["ProductName"].ToString().Trim(), obj["MACAddress"].ToString().Trim(), group[1].Value, group[2].Value));
                    }
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>Software
        /// <para><returns>Возвращает List&lt;Software&gt;</returns></para>
        /// </summary>
        public List<Software> GetSoftware()
        {
            List<Software> result = new List<Software>();
            string Query = "SELECT * FROM Win32_Product";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Add(new Software(obj["Caption"].ToString().Trim()));
                }
            }
            catch (Exception ex)
            {
                Errors.ErrorEventHandler("Source: " + ex.Source + "\nMessage: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }

            return result;
        }

        /// <summary>Data preaparation
        /// <para><returns>Возвращает Computer</returns></para>
        /// </summary>
        public Computer PrepareData()
        {
            List<RAM> array_ram = GetRAM();
            List<NetworkAdapter> array_nw = GetNetworkAdapter();
            Processor pr = GetProcessor();
            ComputerName cn = GetComputerName();
            List<CDROMDrive> array_cdrom = GetCDROM();
            Motherboard mb = GetMotherboard();
            List<Video> array_vd = GetVideo();
            List<DiskDrive> array_dd = GetDiskDrive();
            List<Display> array_dp = GetMonitor();
            List<Software> array_sw = GetSoftware();

            Computer Comp = new Computer();

            Comp._ComputerName = cn;
            Comp._RAM = array_ram.ToArray();
            Comp._NetworkAdapter = array_nw.ToArray();
            Comp._Processor = pr;
            Comp._CDROMDrive = array_cdrom.ToArray();
            Comp._Motherboard = mb;
            Comp._Video = array_vd.ToArray();
            Comp._DiskDrive = array_dd.ToArray();
            Comp._Display = array_dp.ToArray();
            Comp._Software = array_sw.ToArray();
            Comp._Comment = "";
            Comp._Inventory = "";

            return Comp;
        }
    }
}
