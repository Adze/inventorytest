using System;
using System.IO;
using System.Text;
using Client;

namespace Log
{
    public partial class Log
    {
        private const string LogName = "Logs.txt";//��� ����� �����

        /// <summary>
        /// ����������� ������.
        /// </summary>
        public Log()
        {
            Errors.ErrorEventHandler = new Errors.ErrorEvent(this.SaveLog);
        }

        /// <summary>
        /// ���������� ������ � �����
        /// </summary>
        /// <param name="Error">����� ���������</param>
        public void SaveLog(string Error)
        {
            try
            {
                StreamWriter LogsOut_Stream = new StreamWriter(LogName, true);
                LogsOut_Stream.WriteLine(DateTime.Now + " - " + Error);

                LogsOut_Stream.Close();
            }
            catch (IOException e)
            {
                //MessageBox.Show("������ ������ � ���� ����� Logs.txt!\n" + e.Message.ToString());
                return;
            }
        }
    }
}