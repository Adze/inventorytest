using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using Android.Runtime;
using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.Widget;
using Android.OS;
using ZXing;
using ZXing.Mobile;
using Newtonsoft.Json;
using Interfaces;

namespace Sample.Android
{
	[Activity (Label = "Inventory QR-Scanner", MainLauncher = true, ConfigurationChanges=ConfigChanges.Orientation|ConfigChanges.KeyboardHidden)]
	public class Activity1 : Activity
	{
		Button StartScanBtn;
		Button SendBtn;
		EditText ServerAdrText;
		EditText CommentText;
		TextView InventoryNumberLabel;

		MobileBarcodeScanner scanner;
		
		Client_class client;
		DB DataBase;
	
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			//Create a new instance of our Scanner
			scanner = new MobileBarcodeScanner(this);

			InventoryNumberLabel = this.FindViewById<TextView>(Resource.Id.InventoryNumberLabel);
			ServerAdrText = this.FindViewById<EditText> (Resource.Id.ServerAdrText);
			CommentText = this.FindViewById<EditText> (Resource.Id.CommentText);

			client = new Client_class();
			client.Host = "192.168.0.107";
			client.Port = 2000;

			DataBase = new DB ();

			Button flashButton;
			View zxingOverlay;

			StartScanBtn = this.FindViewById<Button>(Resource.Id.StartScanBtn);
			StartScanBtn.Click += async delegate {

				//Tell our scanner we want to use a custom overlay instead of the default
				scanner.UseCustomOverlay = true;

				//Inflate our custom overlay from a resource layout
				zxingOverlay = LayoutInflater.FromContext(this).Inflate(Resource.Layout.ZxingOverlay, null);

				//Find the button from our resource layout and wire up the click event
				flashButton = zxingOverlay.FindViewById<Button>(Resource.Id.buttonZxingFlash);
				flashButton.Click += (sender, e) => scanner.ToggleTorch();

				//Set our custom overlay
				scanner.CustomOverlay = zxingOverlay;

				//Start scanning!
				var result = await scanner.Scan();

				HandleScanResult(result);
			};

			SendBtn = this.FindViewById<Button> (Resource.Id.SendBtn);
			SendBtn.Click += (sender, e) => 
			{
				SendData();
			};
		}

		void HandleScanResult (ZXing.Result result)
		{
			string msg = "";

			if (result != null && !string.IsNullOrEmpty(result.Text))
				msg = "Inventory: " + result.Text;
			else
				msg = "Scanning Canceled!";

			//this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
			InventoryNumberLabel.Text = msg.ToString();
			DataBase.AddRecord (result.Text);
		}

		void SendData ()
		{
			if (client.Connect (true)) 
			{
				client.Common.ChangePrivileges ("Invent", "test_invent");
				//client.Invent.Transfer_InventNumber ();

				string[] InvNum = DataBase.GetRecords ();
				InvNum [0] = CommentText.Text;

				client.Invent.Transfer_InventNumber (InvNum);
			}
		}
	}
}


