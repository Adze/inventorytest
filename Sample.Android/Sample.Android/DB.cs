using System;
using System.IO;
using System.Data;
using Mono.Data.Sqlite;


namespace Sample.Android
{
	public class DB
	{
		SqliteConnection Connection;
		public DB ()
		{
			string dbPath = Path.Combine (Environment.GetFolderPath(Environment.SpecialFolder.Personal), "inventory.db3");
			bool exist = File.Exists (dbPath);
			if (!exist) 
			{
				SqliteConnection.CreateFile (dbPath);
			}
			Connect (dbPath, !exist);
		}

		public void Connect(string dbPath, bool IsFirst)
		{
			Connection = new SqliteConnection ("Data Source=" + dbPath);
			Connection.Open ();
			if (IsFirst) 
			{
				SqliteCommand Command = Connection.CreateCommand ();
				Command.CommandText = "CREATE TABLE inventory_list (" +
				                      "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
				                      "inventory_number text NOT NULL);";
				Command.ExecuteNonQuery ();
			}
		}

		public void Disconnect()
		{
			Connection.Close ();
		}

		public void AddRecord(string invent)
		{
			SqliteCommand Command = Connection.CreateCommand ();
			Command.CommandText = String.Format("INSERT INTO inventory_list (inventory_number)" +
				"VALUES ({0})", invent);
			Command.ExecuteNonQuery ();
		}

		public string[] GetRecords()
		{
			int count=0;
			SqliteCommand Command = Connection.CreateCommand ();
			Command.CommandText = String.Format ("SELECT COUNT(*) FROM inventory_list");
			SqliteDataReader reader = Command.ExecuteReader ();
			while (reader.Read ()) 
			{
				count = int.Parse(reader[0].ToString());
			}

			string[] result = new string[count+1];

			Command.CommandText = String.Format ("SELECT inventory_list FROM inventory");
			reader = Command.ExecuteReader ();
			int i = 1;
			while (reader.Read ()) 
			{
				result [i] = reader [0].ToString ();
				i++;
			}

			return result;
		}
	}
}

